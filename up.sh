#!/bin/bash
script_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

source config.sh
_baseurl="https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/"
_basename="itk-demo-"


source ./utils.sh
main_cmd="up"
main_args=-d
demi_container_tools_up
for pkg  in ${_pkg}
do
  configure_${pkg}
done
