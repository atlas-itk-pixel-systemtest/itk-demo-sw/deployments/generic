#!/bin/bash
script_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
main_cmd=$1
shift
main_args=$@

source config.sh
_baseurl="https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/"
_basename="itk-demo-"


source ./utils.sh

demi container tools up -d
for pkg  in ${_pkg}
do
  configure_${pkg}
done
