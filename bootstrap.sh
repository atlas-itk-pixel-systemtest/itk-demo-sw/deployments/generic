#!/bin/bash
source ./config.sh
_baseurl="https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/"
_basename="itk-demo-"
_git_branch=fix_python_install

function clone_pkgs {
  for pkg in ${_pkg}
  do
    if [ ! -d "${_basename}${pkg}" ]; then
      echo "Cloning ${_basename}${pkg}"
      git clone "${_baseurl}${_basename}${pkg}.git"
    else
      echo "${_basename}${pkg} is already cloned" 
    fi
  done
}

git clone -b ${_git_branch} https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git $HOME/.demi
source ~/.demi/setup.sh
demi python init
demi services init
clone_pkgs

# hacks for now
echo CONFIGDB_BACKUP_PATH: ${CONFIGDB_BACKUP_PATH}

mkdir -p $PWD/.docker/etcd/data
chmod a+w $PWD/.docker/etcd/data
mkdir -p  ${CONFIGDB_BACKUP_PATH}
chmod a+w ${CONFIGDB_BACKUP_PATH}
