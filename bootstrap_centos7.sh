#!/bin/bash
function install_docker {
  if !  command -v docker 2>&1 > /dev/null  ; then
    curl -fsSL https://get.docker.com/ | sudo sh
    sudo usermod -aG docker $USER
    sudo systemctl start docker
    sudo chgrp docker /var/run/docker.sock
    sudo yum install -y docker-compose-plugin
  fi
  sudo chgrp docker /var/run/docker.sock
  sudo yum install -y iptables-services
  sudo yum install -y nmap # for service registry
  sudo systemctl stop firewalld
  sudo systemctl disable firewalld
  sudo systemctl start iptables
  sudo systemctl enable iptables
}

install_docker
docker network create demi
