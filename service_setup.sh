#!/bin/bash
script_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

source config.sh

source ./utils.sh
USE_REGISTRY_CACHE=0
main_args=""
demi_container_tools_up
main_cmd=up_db
configure_configdb
sleep 10
main_cmd="setup_db"
configure_configdb

main_cmd="down_db"
configure_configdb
