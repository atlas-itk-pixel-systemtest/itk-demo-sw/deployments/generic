export USE_REGISTRY_CACHE=1
export ETCD_PORT=2379
export HOSTNAME_POSTFIX="$(hostname -d)"
#export THIS_HOST="$(hostname).${HOSTNAME_POSTFIX}"
export THIS_HOST="$(hostname)"
export ETCD_HOST="${THIS_HOST}"
export CONFIGDB_BACKUP_PATH="$PWD/.docker/db_cache"
export API_WORKERS=4
export API_HOST="${THIS_HOST}"
export UI_HOST="${THIS_HOST}"
export MARIADB_HOST="${THIS_HOST}"
export MARIADB_PORT=3306
export PHPMYADMIN_HOST="${THIS_HOST}"
export MARIADB_USER="configdb"
export MARIADB_PWD="test"
export MARIADB_ROOT_PWD="test"
export CONFIGDB_ADDRESS="http://${THIS_HOST}:5000/api"
#export CONFIGDB_NAME="configdb"

export CONFIGDB_SQLALCHEMY_DATABASE_URI="mysql+pymysql://${MARIADB_USER}:${MARIADB_PWD}@${MARIADB_HOST}:${MARIADB_PORT}/configdb"

_pkg="configdb dashboard daqapi resultviewer"
