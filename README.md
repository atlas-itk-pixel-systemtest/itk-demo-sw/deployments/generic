# Generic Deployment Package for Microservice Hosts

## Getting started
Collection of scripts to bootstrap docker, demi and microservices on a node

```
git clone  https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/generic
cd generic
```

Edit the configuration setup in `config.sh`. Currently
the the hosts are set by running hostname on the local host.
Note: make sure the host uses a different name than `localhost` like
a DNS name.

```
export USE_REGISTRY_CACHE=1
export ETCD_HOST="$(hostname)"     # etcd host 
export ETCD_PORT=2379              # etcd port
export THIS_HOST="$(hostname)"     # host the services run on
export CONFIGDB_BACKUP_PATH=~/.docker/db_cache  # config DB cache

# _pkg="configdb dashboard daqapi resultviewer felix"
_pkg="configdb dashboard daqapi resultviewer"  # packages for this host

```

# Bootstrap Docker and Demi
This needs to be run once. It clones to installation packages from git.
Disabled `firewalld` and installs `iptables` required by docker.
docker and the docker compose plugin are installed as documented here
https://docs.docker.com/engine/install/centos/
`nmap` is installed as it is required by the service registry.
```
./bootstrap.sh
```

# Compose docker images and setup services
This will prompt for the database root password
and account name/password of the database user account.
```
./setup_services.sh
```

# Starting and stopping container up containers
```
./up.sh 
./down.sh
```
