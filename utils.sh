#!/bin/bash

source ~/.demi/setup.sh

function init_service {
  cd $script_dir/$MS_DIR
  echo
  pwd
  export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring 
  echo poetry install
  poetry install
}

function exec_service {
  cd "$script_dir/$MS_DIR"
  echo
  pwd
  echo "API_HOST: $API_HOST"
  echo "API_PORT: $API_PORT"
  echo "UI_HOST: $UI_HOST"
  echo "UI_PORT: $UI_PORT"
  echo "NAME: $NAME"
  echo ./tasks.sh $cmd $args
  ./tasks.sh $cmd $args
}

function exec_ui {
  cd "$script_dir/$MS_DIR/ui"
  echo
  pwd
  echo ./tasks.sh $cmd $args
  ./tasks.sh $cmd $args
}

function configure_dashboard {
  export MS_DIR="itk-demo-dashboard"
  export API_HOST=$THIS_HOST
  export API_PORT=5100
  export UI_HOST=$THIS_HOST
  export UI_PORT=80
  export NAME="Dashboard"
  export cmd=$main_cmd
  export args=$main_args
  init_service
  exec_service
  exec_ui
}

function configure_daqapi {
  export MS_DIR="itk-demo-daqapi"
  export API_HOST=$THIS_HOST
  export API_PORT=5005
  export UI_HOST=$THIS_HOST
  export UI_PORT=85
  export NAME="YARREmu-DAQ-API"
  export cmd=$main_cmd
  export args=$main_args
  init_service
  exec_service
  exec_ui
}

function configure_configdb {
  export MS_DIR="itk-demo-configdb"
  export API_HOST=$THIS_HOST
  export API_PORT=5000
  export UI_HOST=$THIS_HOST
  export UI_PORT=81
  export NAME="Config-DB"
  all="_all"
  [ ${main_cmd} == "setup_db" ] && all=""
  [ ${main_cmd} == "up_db" ] && all=""
  [ ${main_cmd} == "down_db" ] && all=""
  export cmd="${main_cmd}${all}"
  export args=""
  init_service
  exec_service
}

function configure_resultviewer {
  export MS_DIR="itk-demo-resultviewer"
  export API_HOST=$THIS_HOST
  export API_PORT=5002
  export UI_HOST=$THIS_HOST
  export UI_PORT=82
  export NAME="Resultviewer"
  export cmd=$main_cmd
  export args=$main_args
  init_service
  exec_service
  exec_ui
}

function configure_felix {
  export MS_DIR="itk-demo-felix"
  export FELIX_URL="http://$THIS_HOST:8080"
  export API_HOST=$THIS_HOST
  export API_PORT=5010
  export UI_HOST=$THIS_HOST
  export UI_PORT=89
  export NAME="FELIX-UI"
  export cmd=$main_cmd
  export args=$main_args
  init_service
  exec_service
  exec_ui
}

function demi_container_tools_up {
  demi container tools up -d
}

function demi_container_tools_down {
  demi container tools down
}

